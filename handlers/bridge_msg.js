const config = require('../config.json');
const axios = require('axios');

module.exports = {
    message: async obj => {
        let { msg, client, lodestone } = obj;

        if (msg.channel.id != config.lodestone.bridgeChannelId) {
            return;
        }

        if (msg.content.length < 1 && msg.attachments.size == 0) {
            return;
        }

        const tellraw = [
            {
                text: msg.author.discriminator == "0" ? msg.author.username : msg.author.tag, 
                color: msg.member.displayHexColor
            }
        ];

        try {
            if (msg?.reference?.messageId) {
                const reference = await msg.channel.messages.fetch(msg.reference.messageId);
                let replyTo, replyToColor = 'white', replyToText;

                if (reference.author.id == client.user.id) {
                    replyTo = reference.embeds[0].description.split(':')[0].split(' ')[0].replaceAll('*', '');
                    replyToText = reference.embeds[0].description;
                } else {
                    replyTo = reference.author.discriminator == "0" ? reference.author.username : reference.author.tag;
                    replyToColor = reference.member.displayHexColor;
                    replyToText = reference.content ?? '';

                    for (const attachment of reference.attachments.values()) {
                        replyToText += `${replyToText.length > 0 ? ' ' : ''}[${attachment.name}]`;
                    }
                }

                tellraw.push({
                    text: '→',
                    color: 'blue'
                }, {
                    text: replyTo,
                    color: replyToColor,
                    hoverEvent: {
                        action: 'show_text',
                        contents: replyToText
                    }
                });
            }
        } catch(e) {
            console.error(e);
        }

        tellraw.push({
            nospace: true,
            text: ':',
            color: 'gray'
        });

        if (msg.content != null) {
            for (const [i, piece] of msg.content.split(' ').entries()) {
                try {
                    const url = new URL(piece);

                    if (['http:', 'https:', 'ftp:'].includes(url.protocol) === false) {
                        throw 'Not a website';
                    }

                    tellraw.push({
                        color: 'aqua',
                        text: piece,
                        clickEvent: {
                            action: 'open_url',
                            value: piece
                        }
                    });
                } catch(e) {
                    if (piece.startsWith('<:') || piece.startsWith('<a:')) {
                        try {
                            const name = piece.split(':')[1];
                            const id = piece.split(':')[2].split('>')[0];

                            tellraw.push({
                                color: '#bcbaba',
                                text: `:${name}:`,
                                clickEvent: {
                                    action: 'open_url',
                                    value: `https://cdn.discordapp.com/emojis/${id}`
                                }
                            });
                        } catch(e) {
                            tellraw.push({
                                color: 'white',
                                text: piece
                            });
                        }
                    } else {
                        tellraw.push({
                            color: 'white',
                            text: piece
                        });
                    }
                }
            }
        }

        if (msg.attachments.size > 0) {
            for (const attachment of msg.attachments.values()) {
                tellraw.push({
                    text: `[${attachment.name}]`,
                    color: 'dark_aqua',
                    clickEvent: {
                        action: 'open_url',
                        value: attachment.url
                    }
                });
            }
        }

        for (const element of tellraw.slice(1)) {
            if (element.nospace) {
                delete element.nospace;
                continue;
            }

            if (typeof element.text !== 'string') {
                continue;
            }

            element.text = ` ${element.text}`;
        }

        const payload = `tellraw @a ${JSON.stringify(tellraw)}`;

        await axios.post(`${lodestone.host}/api/v1/instance/${lodestone.instanceId}/console`, payload, {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${lodestone.token}`
            }
        });
    }
};
