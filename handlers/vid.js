const config = require('../config.json');
const helper = require('../helper.js');
const util = require('util');
const fs = require('fs').promises;
const axios = require('axios');
const { inlineCode } = require('@discordjs/builders');
const { ActionRowBuilder, ButtonBuilder, ButtonStyle } = require('discord.js');

const path = require('path');
const child_process = require('child_process');

const execFile = util.promisify(child_process.execFile);

module.exports = {
    message: async obj => {
        let { msg } = obj;

        const argv = msg.content.split(" ");

        let UPLOAD_LIMIT = 25 * 1000 * 1000;

        if (msg.guild !== null) {
            const serverBoosts = msg.guild.premiumSubscriptionCount;

            if (serverBoosts >= 14)
                UPLOAD_LIMIT = 100 * 1000 * 1000;
            else if (serverBoosts >= 7)
                UPLOAD_LIMIT = 50 * 1000 * 1000;
        }

        const sites = [
            "tiktok.com",
            "https://redd.it",
            "https://v.redd.it",
            "reddit.com",
            "https://t.co",
            "facebook.com",
            "instagram.com",
            "nicovideo.jp/watch",
            "https://twitter.com",
            "https://mobile.twitter.com"
        ];

        const sitesDelEmbed = [
            "https://redd.it",
            "https://v.redd.it",
            "reddit.com",
            "https://twitter.com",
            "https://mobile.twitter.com",
            "https://vm.tiktok.com",
            "https://www.tiktok.com",
            "https://tiktok.com",
            "instagram.com"
        ];

        for (let arg of argv) {
            if (arg.startsWith('https://') && sites.some(a => arg.includes(a)) && !arg.includes('.mp4')) {
                arg = arg.replaceAll('fxtwitter.com', 'twitter.com');

                if (arg.includes('tiktok.com/t/'))
                    arg = arg.replaceAll('www.tiktok.com', 'vm.tiktok.com');

                try {
                    let fileName = (await execFile('yt-dlp', ['-o', '%(id)s.%(ext)s', '-f', 'best[vcodec!=h265]/bestvideo*[vcodec!=h265]+bestaudio/best', '--get-filename', arg])).stdout.trim();

                    msg.react('705628243830636574').catch(console.error);

                    const dlPath = `./downloads/_${fileName}`;

                    await execFile('yt-dlp', ['--write-info-json', '-o', `${dlPath}`, '-f', 'best[vcodec!=h265]/bestvideo*[vcodec!=h265]+bestaudio/best', arg]);

                    const ffArgs = [
                        '-y',
                        '-i', dlPath,
                        '-c', 'copy',
                        '-movflags', '+faststart'
                    ];

                    if (argv.includes('-an')) {
                        ffArgs.push('-an');
                    }

                    let uploadPath = dlPath.replace('/_', '/');

                    ffArgs.push(uploadPath);

                    await execFile('ffmpeg', ffArgs)

                    const stat = await fs.stat(uploadPath);
                    const info = JSON.parse(await fs.readFile(`./downloads/${path.basename(dlPath, path.extname(dlPath))}.info.json`));
                    let row;

                    if (info.extractor == 'TikTok' && info.width == 0 && info.height == 0) {
                        //TODO: self-host https://github.com/wukko/cobalt
                        const response = await axios.post('https://co.wukko.me/api/json', { url: arg }, { 
                            headers: { 
                                'Accept': 'application/json' 
                            }
                        });

                        const images = response.data.picker;

                        if (images == null || images.length == 0) {
                            return;
                        }

                        const imgDlPath = `./downloads/${info.id}`;
                        const imgDlRsPath = `./downloads/${info.id}rs`;
                        
                        await fs.mkdir(imgDlPath, { recursive: true });
                        await fs.mkdir(imgDlRsPath, { recursive: true });

                        const downloadImages = [];
                        const imagePaths = [];

                        for (const [index, imageUrl] of images.map(a => a.url).entries()) {
                            const url = new URL(imageUrl);
                            const imageName = `${index}${path.extname(url.pathname)}`;
                            imagePaths.push(`${imgDlRsPath}/${imageName}`);
                            downloadImages.push(execFile('curl', ['-o', `${imgDlPath}/${imageName}`, url]));
                        }
 
                        await Promise.all(downloadImages);

                        await execFile('magick', ['mogrify', '-resize', '1080x1920', '-gravity', 'center', '-background', 'black', '-extent', '1080x1920', '-path', imgDlRsPath, `${imgDlPath}/*`]);

                        const slideshowPath = uploadPath.replace(path.extname(uploadPath), '') + '_s' + path.extname(uploadPath);

                        const slideshowArgs = [];
                        let filterComplex = '';

                        for (const image of imagePaths) {
                            slideshowArgs.push('-loop', 1, '-t', 2.925, '-i', image);
                        }

                        for (let i = 0; i < imagePaths.length - 1; i++) {
                            if (i == 0) {
                                filterComplex += '[0][1]';
                            } else {
                                filterComplex += `[f${i}][${i + 1}]`;
                            }

                            filterComplex += `xfade=transition=slideleft:duration=0.35:offset=${2.575 * (i + 1)}`;
                            
                            if (i < imagePaths.length - 2) {
                                filterComplex += `[f${i+1}];`;
                            } else {
                                filterComplex += ',format=yuv420p[v]';
                            }
                        }

                        slideshowArgs.push('-i', uploadPath, '-filter_complex', `"${filterComplex}"`, 
                        '-map', '"[v]"', '-map', `${imagePaths.length}:a`,
                        '-c:v', 'libx264', '-movflags', '+faststart', '-r', 60,
                        '-c:a', 'aac', '-b:a', '128k', '-shortest', '-y', slideshowPath);

                        await execFile('ffmpeg', slideshowArgs, { shell: true });
                        uploadPath = slideshowPath;

                        row = new ActionRowBuilder()
                        .addComponents(
                            new ButtonBuilder()
                                .setCustomId(`get-images-${info.id}`)
                                .setLabel(`${imagePaths.length} Images`)
                                .setEmoji('🖼️')
                                .setStyle(ButtonStyle.Primary),
                        );
                    }

                    const embeds = [];

                    let content;

                    if (sitesDelEmbed.some(a => arg.includes(a))) {
                        switch(info.extractor) {
                            case 'Reddit':
                                const sub = info.webpage_url.split("/").slice(3, 5).join('/');
                                content = `${inlineCode(sub)} – ${inlineCode(info.fulltitle)}`;
                                break;
                            case 'TikTok':
                                const desc = info.description.split(" ");
                                const filtered_desc = desc.filter(a => a.trim().charAt('0') != '#');

                                if (filtered_desc.length > 0) {
                                    content = `${inlineCode('@' + info.uploader)} - ${inlineCode(filtered_desc.join(' '))}`;
                                }
                                break;
                            case 'twitter':
                                let tweet = info.description.split(" ");
                                tweet.pop();

                                const tweet_text = tweet.join(' ');

                                /*
                                embeds.push({
                                    color: 0x1da1f2,
                                    author: {
                                        url: info.uploader_url,
                                        name: `${info.uploader} (@${info.uploader_id})`
                                    },
                                    description: tweet,
                                    footer: {
                                        icon_url: 'https://abs.twimg.com/icons/apple-touch-icon-192x192.png',
                                        text: 'Twitter'
                                    },
                                    fields: [{
                                        name: 'Retweets',
                                        value: info.repost_count.toString(),
                                        inline: true
                                    }, {
                                        name: 'Likes',
                                        value: info.like_count.toString(),
                                        inline: true
                                    }, {
                                        name: 'Replies',
                                        value: info.comment_count.toString(),
                                        inline: true
                                    }],
                                    timestamp: info.timestamp * 1000
                                });*/

                                if (tweet.length > 0) {
                                    content = `${inlineCode('@' + info.uploader_id)} - ${inlineCode(tweet_text)}`;
                                }
                                break;
                            case 'html5':
                            case 'generic':
                            case 'youtube':
                                return;
                            default:
                                content = `${inlineCode(info.uploader)} – ${inlineCode(info.fulltitle)}`;
                        }
                    }

                    const components = row ? [row] : [];

                    const allowedMentions = { repliedUser: false };

                    if (stat.size >= UPLOAD_LIMIT) {
                        const response = await execFile('curl',
                            [
                                '-s', '-X', 'POST', config.chibiUploadEndpoint,
                                '-H', 'content-type: multipart/form-data',
                                '-H', 'accept: application/vnd.chibisafe.json',
                                '-H', 'token: q9cLZMeU0aueeTqgKhI8iGSzHLUlSGD2E1q7M17QuZWkTukWwdFmXR9XsLJOB1lu',
                                '-F', `files[]=@${uploadPath}`
                            ]);

                        const json = JSON.parse(response.stdout);

                        json.url = json.url.replace('http://localhost:5000', 'https://boob.li');

                        await msg.reply({ content: content !== undefined ? (content + ` (${json.url})`) : json.url, embeds, allowedMentions, components });
                    } else {
                        if (embeds.length > 0) {
                            await msg.reply({
                                content,
                                embeds,
                                allowedMentions
                            });

                            await msg.channel.send({
                                files: [{
                                    attachment: uploadPath,
                                    name: fileName,

                                }],
                            });
                        } else {
                            await msg.reply({
                                content,
                                files: [{
                                    attachment: uploadPath,
                                    name: fileName,

                                }],
                                components,
                                allowedMentions
                            });
                        }

                    }

                    if (sitesDelEmbed.some(a => arg.includes(a))) {
                        msg.suppressEmbeds(true).catch(console.error);
                    }
                } catch (e) {
                    console.error(e);
                } finally {
                    msg.reactions.cache.find(a => a.me).remove().catch(console.error);
                }

                break;
            }
        }
    }
};
