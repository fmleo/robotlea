const fs = require('fs');

const configDefault = {
    "credentials": {
        "discord_client_id": "",
        "bot_token": "",
        "sky_api_key": ""
    },
    "lodestone": {
        "bridgeChannelId": "",
        "username": "",
        "password": "",
        "host": "",
        "instanceId": ""
    },
    "mainGuild": "680401335862296611",
    "sky_api_base": "https://sky.shiiyu.moe",
    "debug": false,
    "prefix": "!",
    "chibiUploadEndpoint": "http://localhost:5000/api/upload",
    "dbUri": "redis://localhost:6379",
    "dbNamespace": "robotlea"
};

const emotesDefault = {};
const subForumsDefault = {};
const mayorDataDefault = {};

if(!fs.existsSync('./config.json'))
    fs.writeFileSync('./config.json', JSON.stringify(configDefault, null, 4));

if(!fs.existsSync('./emotes.json'))
    fs.writeFileSync('./emotes.json', JSON.stringify(emotesDefault, null, 4));

if(!fs.existsSync('./subForums.json'))
    fs.writeFileSync('./subForums.json', JSON.stringify(subForumsDefault, null, 4));

if(!fs.existsSync('./mayorData.json'))
    fs.writeFileSync('./mayorData.json', JSON.stringify(mayorDataDefault, null, 4));
