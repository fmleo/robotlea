const axios = require('axios');
const helper = require('../helper');
const { DateTime } = require("luxon");
const { ActionRowBuilder, SelectMenuBuilder } = require('discord.js');

const types = [
    "Carrot",
    "Nether Wart",
    "Potato",
    "Cocoa Beans",
    "Melon",
    "Wheat",
    "Pumpkin",
    "Cactus",
    "Mushroom",
    "Sugar Cane"
];

const shortName = name => {
    switch(name) {
        case 'Cocoa Beans':
            return 'Cocoa';
        case 'Nether Wart':
            return 'Wart';
        case 'Sugar Cane':
            return 'Cane';
        default:
            return name;
    }
}

const products = {};

for (const type of types) {
    products[type] = { name: type, tag: type.toLowerCase().split(' ') };
}

module.exports = {
    command:  ['contests', 'contest', 'jacobs'],
    description: "Look up upcoming farming contests.",
    argsRequired: 0,
    usage: '[type]',
    example: {
        run: "contests carrot",
        result: "Shows upcoming cocoa contests."
    },
    call: async obj => {
        const { argv, msg, client, prefix, jacobsContests } = obj;

        const cropEmote = crop => helper.emote(crop.toUpperCase().replaceAll(' ', '_'), msg.guild, client);

        let filterCrop;

        if (argv.length > 1) {
            const cropQuery = argv.slice(1).join(' ');
            filterCrop = helper.getBazaarProduct(cropQuery, products).name;
        }

        if (jacobsContests.length == 0) {
            throw "Failed to fetch contests.";
        }

        let upcoming = jacobsContests.filter(a => a.time >= Date.now() / 1000 - 20*60);

        if (filterCrop) {
            upcoming = upcoming.filter(a => a.crops.includes(filterCrop));
        }

        if (upcoming.length == 0) {
            throw "No contests available, maybe the SkyBlock year just started.";
        }

        const embed = {
            title: 'Farming Contests',
            thumbnail: {
                url: 'https://wiki.hypixel.net/images/c/ce/Minecraft_items_golden_hoe.png'
            },
            description: '',
            footer: {
                text: `dawjaw.net${helper.sep}${prefix}contests [crop]`
            }
        };

        if (filterCrop != null) {
            embed.description = `Only showing: ${cropEmote(filterCrop)} ${shortName(filterCrop)}\n\n`;
        }

        const selectMenu = new SelectMenuBuilder()
        .setCustomId('set-contest-reminder')
        .setPlaceholder('Sewect contest to wemind you for');

        for (let i = 0; i < Math.min(upcoming.length, 6); i++) {
            if (i > 0) {
                embed.description += '\n';
            }

            const contest = upcoming[i];

            const crops = [];
            const cropsNE = [];

            for (const crop of contest.crops) {
                crops.push(`${cropEmote(crop)} ${shortName(crop)}`);
                cropsNE.push(shortName(crop));
            }

            if (contest.time > Date.now() / 1000) {
                selectMenu.addOptions({
                    label: `${cropsNE.join(' / ')} – ${DateTime.fromSeconds(contest.time).toRelative()}`,
                    value: `remind-contest-${contest.time}`
                });
            }            

            embed.description += `${crops.join(' / ')} – <t:${contest.time}:R>.`;
        }

        const row = new ActionRowBuilder()
        .addComponents(selectMenu);

        return { embeds: [embed], components: [row] };
    }
};
