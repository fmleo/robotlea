const helper = require('../helper');
const config = require('../config.json');
const axios = require('axios');
const { CancelToken } = axios;
const _ = require('lodash');

const filterBy = (value, str) => Object.keys(value).filter(
    item => new RegExp('^' + str.replace(/\*/g, '.*') + '$').test(item)
);

module.exports = {
    command: ['count'],
    argsRequired: 2,
    description: [
        "Count items for player.",
    ],
    example: [
        {
            run: "count leaphant sludge_juice",
            result: "Count Sludge Juice for LeaPhant."
        }
    ],
    usage: '<username> [item]',
    call: async obj => {
        const { guildId, argv, client, msg, prefix, responseMsg, endEmitter } = obj;

        const footer = {
            icon_url: "https://cdn.discordapp.com/attachments/572429763700981780/1041778058056704100/skycrypt_logo_round.png",
            text: `sky.shiiyu.moe${helper.sep}${prefix}count <username> <item id> [profile]`
        }

        const msgObj = {
            embeds: [{
                color: helper.mainColor,
                author: {
                    name: `${argv[1]}`
                },
                footer,
                description: `Awaiting API response... ${helper.emote('beespin', null, client)}`
            }]
        };

        let message = responseMsg;

        if (responseMsg)
            await responseMsg.edit(msgObj);
        else
            message = await msg.channel.send(msgObj);

        const source = CancelToken.source();

        axios.get(
            `${config.sky_api_base}/api/v2/profile/${argv[1]}`,
            {
                params: { key: config.credentials.sky_api_key },
                cancelToken: source.token
            }
        ).then(async response => {
            const { data } = response;

            let profile = data.profiles[_.findKey(data.profiles, a => a.current)];
            let customProfile;

            // Could handle more than 4 arguments and tell the user to unstupid but shrug
            if (argv.length > 3) {
                customProfile = argv[3].toLowerCase();

                for (const key in data.profiles)
                    if (data.profiles[key].cute_name.toLowerCase() == customProfile) {
                        profile = data.profiles[key];
                        break
                    }
            }

            /* for (const key in profile.items) {
                const items = profile.items[key];

                if (!Array.isArray(items)) {
                    continue;
                }

                all_items.push(...items);
            } */
            const { items } = profile;

            // Could check if profile or items is null just in case to stop processing but meh

            // Apparently SkyCrypt returns an empty array for items blocked by API settings,
            // but having the nullish op is fine too, just in case
            const all_items = {
                "Armor": items?.armor ?? [],
                "Equipment": items?.equipment ?? [],
                "Wardrobe": items?.wardrobe_inventory ?? [],
                "Inventory": items?.inventory ?? [],
                "Ender Chest": items?.enderchest ?? [],
                "Storage": _.flatten(items?.storage?.map(it => it.containsItems)), // Surprisingly, _.flatten(undefined) returns []
                "Personal Vault": items?.personal_vault ?? [],
                "Sacks": [], // JS keeps key iteration order so keep this here, process later.
                "Accessory Bag": items?.accessory_bag ?? [],
                "Fishing Bag": items?.fishing_bag ?? [],
                "Quiver": items?.quiver ?? [],
                "Potion Bag": items?.potion_bag ?? [],
                "Candy Bag": items?.candy_bag ?? []
            }

            let sum = 0
            Object.keys(all_items).forEach(k => {
                const amount = all_items[k]
                    .filter(item => item?.tag?.ExtraAttributes?.id == argv[2].toUpperCase())
                    .map(item => item.Count ?? 1)
                    .reduce((acc, num) => acc + num, 0)
                if (amount > 0) {
                    sum += amount
                    all_items[k] = amount
                } else {
                    delete all_items[k]
                }
            })

            if (argv[2].toUpperCase() in (profile.raw?.sacks_counts ?? {})) {
                const amount = profile.raw.sacks_counts[argv[2].toUpperCase()]
                all_items["Sacks"] = amount
                sum += amount
            }

            let description = '';

            if (sum == 0) {
                description += `\`${profile.data.display_name} has none of this item.\``;
            } else {
                const keys = Object.keys(all_items)
                if (keys.length > 1) {
                    let tooltip = '';
                    keys.forEach(k => {
                        tooltip += `${k}: ${all_items[k]}\n`;
                    })
                    description += `\`Total\`: \`${sum}\` [(hover for sources)](${message.url} "${tooltip}")`;
                } else {
                    keys.forEach(k => {
                        description += `\`${k}\`: \`${all_items[k]}\`\n`;
                    })
                }
            }

            const embed = {
                color: helper.mainColor,
                url: `https://sky.shiiyu.moe/stats/${profile.data.uuid}/${profile.data.profile.profile_id}`,
                author: {
                    icon_url: `https://minotar.net/helm/${profile.data.uuid}/64?${new Date().toISOString().split('T')[0]}`,
                    name: `${profile.data.display_name}'s API (${profile.cute_name})`,
                    url: `https://sky.shiiyu.moe/stats/${profile.data.uuid}/${profile.data.profile.profile_id}`,
                },
                footer,
                description
            };

            await message.edit({ embeds: [embed] });
        }).catch(console.error);

        return message;
    }
};
