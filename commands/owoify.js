const helper = require('../helper.js');

module.exports = {
    command: ['uwuify', 'owoify'],
    argsRequired: 0,
    call: async obj => {
        const { msg, argv } = obj;

        let input = "", output = "";

        if(argv.length > 1){
            input = argv.slice(1).join(" ");
        }else if(msg.reference){
            const referenceMsg = await msg.channel.messages.fetch(msg.reference.messageId);

            input = referenceMsg.content;
        }else{
            const lastMessages = msg.channel.messages.cache.sort((a, b) => a.createdAt - b.createdAt);

            lastMessages.forEach(message => {
                console.log(message.content);

                if(message.author.bot === false && message.content.startsWith('!') === false){
                    input = message.content;
                    return;
                }
            });
        }

        const replacements = [
            ["you", "yuw"],
            ["fuck", "fack"],
            ["fuk", "fak"],
            ["na", "nya"],
            ["no", "nyo"],
            ["ne", "nye"],
            ["nu", "nyu"],
            ["hi", "hii"],
        ];

        for(let i = 0; i < input.length; i++){
            let char = input.charAt(i);

            switch(char){
                case 'r':
                case 'l':
                    output += 'w';
                    break;
                case 'R':
                case 'L':
                    output += 'W';
                    break;
                case 't':
                case 'T':
                    let check = input.charAt(i + 1);

                    if(check.toLowerCase() == 'h'){
                        if(char == 't')
                            output += 'd';
                        else
                            output += 'D';
                        i++;
                    }else{
                        output += char;
                    }

                    break;
                default:
                    output += char;
            }
        }

        for(const r of replacements){
            output = output.replaceAll(r[0], r[1]);
            output = output.replaceAll(r[0].toUpperCase(), r[1].toUpperCase());
        }

        output = output.trim() + " uwu";

        return { embeds: [{
            description: output,
            color: 0xb4327d,
            footer: {
                text: `Requested by ${msg.author.tag}`,
                icon_url: msg.author.avatarURL(),
            }
        }] };
    }
};
