const helper = require('../helper');
const config = require('../config.json');
const axios = require('axios');
const { CancelToken } = axios;
const _ = require('lodash');

const filterBy = (value, str) => Object.keys(value).filter(
    item => new RegExp('^' + str.replace(/\*/g, '.*') + '$').test(item)
);

module.exports = {
    command: ['api'],
    argsRequired: 1,
    description: [
        "Get raw API response path.",
    ],
    example: [
        {
            run: "api leaphant.stats.",
            result: "Returns forge for LeaPhant."
        }
    ],
    usage: '<path> [profile]',
    call: async obj => {
        const { guildId, argv, client, msg, prefix, responseMsg, endEmitter } = obj;

        const footer = {
            icon_url: "https://cdn.discordapp.com/attachments/572429763700981780/1041778058056704100/skycrypt_logo_round.png",
            text: `sky.shiiyu.moe${helper.sep}${prefix}api <path> [profile]`
        }

        const msgObj = {
            embeds: [{
                color: helper.mainColor,
                author: {
                    name: `${argv[1]}`
                },
                footer,
                description: `Awaiting API response... ${helper.emote('beespin', null, client)}`
            }]
        };

        let message = responseMsg;

        if(responseMsg)
            await responseMsg.edit(msgObj);
        else
            message = await msg.channel.send(msgObj);

        const source = CancelToken.source();

        let path = argv[1].split('.');
        let username = path[0];

        axios.get(
            `${config.sky_api_base}/api/v2/profile/${username}`, 
            { 
                params: { key: config.credentials.sky_api_key },
                cancelToken: source.token
            }
        ).then(async response => {
            const { data } = response;

            let profile = data.profiles[_.findKey(data.profiles, a => a.current)];
            let customProfile;

            if(argv.length > 2){
                customProfile = argv[2].toLowerCase();

                for(const key in data.profiles)
                    if(data.profiles[key].cute_name.toLowerCase() == customProfile)
                        profile = data.profiles[key];
            }

            let type = 'raw';

            if (path.at(-1) == 'length') {
                type = 'length';
                path.pop();
            }

            let match;

            if (path.at(-1).includes('*')) {
                type = 'wildcard';
                match = path.pop();
            }

            const value = helper.getPath(profile, 'raw', ...path.slice(1));

            let description;
            let responseValue;

            if (type == 'raw') {
                responseValue = JSON.stringify(value, null, 2);
            } else if (type == 'wildcard') {
                const keys = filterBy(value, match);

                const responseObj = {};

                for (const key of keys) {
                    responseObj[key] = value[key];
                }

                responseValue = JSON.stringify(responseObj, null, 2);
            } else if (type == 'length') {
                responseValue = value.length;
            }

            description = `\`${argv[1]}\`: \`\`\`JSON\n${responseValue}\`\`\``;

            const embed = {
                color: helper.mainColor,
                url: `https://sky.shiiyu.moe/stats/${profile.data.uuid}/${profile.data.profile.profile_id}`,
                author: {
                    icon_url: `https://minotar.net/helm/${profile.data.uuid}/64`,
                    name: `${profile.data.display_name}'s API (${profile.cute_name})`,
                    url: `https://sky.shiiyu.moe/stats/${profile.data.uuid}/${profile.data.profile.profile_id}`,
                },
                footer,
                description
            };

            await message.edit({ embeds: [embed] });
        }).catch(console.error);

        return message;
    }
};
