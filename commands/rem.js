const helper = require('../helper.js');
const _ = require('lodash');
const { ActionRowBuilder, ButtonBuilder, ButtonStyle, SelectMenuBuilder } = require('discord.js');

const UNITS = new Object();

UNITS.s = 1;
UNITS.m = UNITS.s * 60;
UNITS.h = UNITS.m * 60,
UNITS.d = UNITS.h * 24;
UNITS.w = UNITS.d * 7;
UNITS.f = UNITS.w * 2;
UNITS.mo = UNITS.d * 30;
UNITS.y = UNITS.d * 365;

const getList = (userId, guildId, reminders) => {
    const userReminders = reminders.filter(r => r.author == userId);
    const guildReminders = userReminders.filter(r => 
       (guildId == null && r.guild == null) 
    || (guildId != null && r.guild == guildId)
    );

    if (guildReminders.length == 0) {
        return {
            embeds: [{
                color: helper.errorColor,
                description: 'You have no weminders set. (T＿T)'
            }]
        };
    }

    const embed = {
        color: helper.mainColor,
        title: 'Your Weminders',
        description: ''
    };

    for (const [index, reminder] of guildReminders.entries()) {
        if (index > 0) {
            embed.description += '\n';
        }

        embed.description += `• \`REM${reminder.id}\``;

        if (reminder.with) {
            embed.description += `: ${_.truncate(reminder.with)}`;
        }

        embed.description += ` (<t:${Math.floor(reminder.at / 1000)}:R>)`;
    }

    const selectMenu = new SelectMenuBuilder()
    .setCustomId('delete-reminder')
    .setPlaceholder('Sewect weminder to dewete');

    for (const reminder of guildReminders) {
        let label = `REM${reminder.id}`;

        if (reminder.with) {
            label += `: ${_.truncate(reminder.with)}`;
        }

        selectMenu.addOptions({
            label,
            value: reminder.id.toString()
        });
    }

    const row = new ActionRowBuilder()
    .addComponents(selectMenu);

    return { 
        embeds: [embed],
        components: [row]
    };
};

module.exports = {
    getList,
    command: ['rem', 'wem', 'remind', 'wemind'],
    argsRequired: 1,
    description: [
        "Set a weminder or list your weminders.",
        "Valid time units: `y` Year `mo` Month (`30d`) `f` Fortnight",
        "`w` Week `d` Day `h` Hour `m` Minute `s` Second"
    ],
    example: [
        {
            run: "rem 2d4h you forgor to turn off the oven",
            result: "Sets weminder in 2 days and 4 hours."
        },
        {
            run: "rem list",
            result: "Lists your weminders and allows deletion."
        }
    ],
    usage: '<time or "list"> [reminder text]',
    call: async obj => {
        const { guildId, msg, argv, db } = obj;

        const reminders = await db.get('reminders') ?? [];

        if (argv[1] == 'list') {
            await msg.channel.send(getList(msg.author.id, guildId, reminders));

            return;
        } else {
            let at;

            if (!isNaN(Number(argv[1]))) {
                at = Math.floor(Number(argv[1])) * 1000;
            } else if (argv[1].startsWith('<t:')) {
                const timestamp = parseInt(argv[1].split(':')?.[1]);

                if (isNaN(timestamp)) {
                    throw "Invalid timestamp.";
                }
                
                at = Math.floor(timestamp) * 1000;
            } else {
                const durations = argv[1].split(/[a-z]+/);
                const units = argv[1].split(/[0-9\.]+/);

                if (durations.length == 0 || units.length < durations.length) {
                    throw "Invalid time.";
                }

                at = Date.now();

                for (const [index, duration] of durations.slice(0, -1).entries()) {
                    const u = units[index + 1];
                    const unit = UNITS[u];

                    if (u == null) {
                        throw "No unit given.";
                    }

                    if (unit == null) {
                        throw `Invalid unit \`${u}\`.`;
                    }

                    const seconds = Number(duration) * unit;

                    at += seconds * 1000;
                }
            }

            let id = reminders.length > 0 ? reminders.at(-1).id + 1 : 0;

            const row = 
            new ActionRowBuilder()
            .addComponents(
                new ButtonBuilder()
                    .setCustomId(`remindme-${id}`)
                    .setLabel('Remind me too!')
                    .setEmoji('🔔')
                    .setStyle(ButtonStyle.Primary),
            );

            const reminderMsg = await msg.channel.send({ 
                embeds: [{ 
                    color: helper.mainColor, 
                    description: `Weminding you <t:${Math.floor(at / 1000)}:R>. („• ᴗ •„)` 
                }],
                components: guildId ? [row] : []
            });

            let reminderText = 
            msg.cleanContent.split(' ').slice(2).join(' ')
            .replaceAll('@everyone', '@evewyone')
            .replaceAll('@here', '@hewe');

            reminders.push({
                id,
                msg: reminderMsg.id,
                cc: [],
                guild: guildId,
                author: msg.author.id,
                at,
                in: msg?.channel?.id ?? msg.author.id,
                with: argv.length > 2 ? reminderText : null
            });

            await db.set('reminders', reminders);

            return;
        }
    }
};
