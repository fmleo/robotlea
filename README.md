# Robot Lea <img align="right" width="100" height="100" src="icon.png">
Discord bot for the 🌈.lea.moe Discord.

<h3>Prerequisites</h3>

- <a href="https://nodejs.org/">Node.js</a>

<h3>Installation</h3>

Clone the project and and run `npm i` to install the dependencies.

Now open `config.json` and enter a valid Discord bot token which you can obtain <a href="https://discord.com/developers/applications/">Here</a>.

You can now run `npm start` to start the bot. Invite the instance to your server by finding the bot's Client ID in your Discord developer console and pasting it into this Link: `https://discord.com/oauth2/authorize?client_id=CLIENT_ID&scope=bot&permissions=1073750016`.
